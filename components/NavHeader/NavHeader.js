// components/NavHeader/NavHeader.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        title:{
            type:String,
            value:'默认title'
        },
        subTitle:{
            type:String,
            value:'默认子标题'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})