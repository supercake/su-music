// pages/center/center.js
import request from '../../utils/request'
let startY = 0
Page({
    startHandler(e) {
        this.setData({
            convertTransition: ''
        })
        startY = e.touches[0].clientY
    },
    moveHandler(e) {
        let positionY = e.touches[0].clientY - startY
        if (positionY < 0) return positionY = 0
        if (positionY > 80) positionY = 80
        this.setData({
            convertTransform: `translateY(${positionY}rpx)`
        })
    },
    endHandler(e) {
        this.setData({
            convertTransform: `translateY(0rpx)`,
            convertTransition: 'transform 0.5s linear'
        })
    },
    toLogin() {
        if (!this.data.userInfo) {
            wx.navigateTo({
                url: '../login/login',
            })
        }
    },
    async getRecentListData() {
        const {
            userId: uid
        } = this.data.userInfo
        const res = await request({
            url: 'user/record',
            data: {
                uid,
                type: 1
            },
        })
        // 如果code是200的话
        if(res.code === 200){
            this.setData({
                recentList: res.weekData.slice(0, 20)
            })
        }
    },

    /**
     * 页面的初始数据
     */
    data: {
        positionY: 0,
        convertTransform: 'translateY(0rpx)',
        convertTransition: '',
        userInfo: '',
        recentList:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        const userInfo = wx.getStorageSync('userInfo')
        if (userInfo) {
            this.setData({
                userInfo: JSON.parse(userInfo)
            })
        }
        // 获取历史播放
        this.getRecentListData() 
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})