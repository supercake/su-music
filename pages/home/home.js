// pages/home/home.js
import request from '../../utils/request'
Page({
    // 轮播图
    async getBannerListData() {
        const bannerList = await request({
            url: 'banner',
            data: {
                type: 2
            }
        })
        if (bannerList.code === 200) {
            this.setData({
                bannerList: bannerList.banners
            })
        }
    },
    // 推荐歌单
    async getSongListData() {
        const res = await request({
            url: 'personalized',
            data: {
                limit: 10
            }
        })
        if (res.code === 200) {
            this.setData({
                songList: res.result
            })
        }
    },
    // 排行榜
    async getRankingListData() {
        let count = 0
        const rankingList = []
        while (count < 5) {
            let rankingInfo = await request({
                url: "top/list",
                data: {
                    idx: count
                }
            })
            let rankingItem = {
                id:rankingInfo.playlist.id,
                rankingName: rankingInfo.playlist.name,
                tracks: rankingInfo.playlist.tracks.slice(0, 3)
            }
            rankingList.push(rankingItem)
            count++
        }
        this.setData({
            rankingList
        })
    },
    /**
     * 页面的初始数据
     */
    data: {
        // 轮播图
        bannerList: [],
        // 推荐歌单
        songList: [],
        // 排行榜
        rankingList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // 轮播图
        this.getBannerListData()
        // 推荐歌单
        this.getSongListData()
        // 排行榜
        this.getRankingListData()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})