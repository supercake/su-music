// pages/login/login.js
import request from '../../utils/request'
Page({
    inputHandler(e) {
        console.log(e);
        const type = e.currentTarget.id
        this.setData({
            [type]: e.detail.value
        })
    },
    async toLogin() {
    // toLogin() {
        const {
            phone,
            password
        } = this.data
        if (!phone) {
            wx.showToast({
                title: '手机号码不能为空',
                icon: 'none'
            })
            return
        }
        const phoneReg = /^(?:(?:\+|00)86)?1(?:(?:3[\d])|(?:4[5-79])|(?:5[0-35-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\d])|(?:9[1589]))\d{8}$/
        if (!phoneReg.test(phone)) {
            wx.showToast({
                title: '手机号码格式不对',
                icon: 'none'
            })
            return
        }
        if (!password) {
            wx.showToast({
                title: '密码不能为空',
                icon: 'none'
            })
            return
        }
        // 向服务器端发送请求 进行正常登陆 
        // const res = await request({
        //     // code:200,
        //     url: 'login/cellphone',
        //     data: this.data
        // })
        // 模拟数据
        const res = {
            code:200,
            profile: {
                avatarUrl: "http://p1.music.126.net/oSlCXajZaSy_BTOKUtmMlg==/109951165455331695.jpg?param=180y180",
                nickname: "酥饼哟i",
                userId: 473312529
            }
        }
        if (res.code === 200) {
            // 本地存储传递登录成功后的数据给个人中心页面
            // 序列化：就是把对象或者是数组转换成json格式的字符串
            wx.setStorageSync('userInfo', JSON.stringify(res.profile))
            wx.reLaunch({
                url: '../center/center',
            })
        }
    },
    /**
     * 页面的初始数据
     */
    data: {
        phone: '15059521191',
        password: 'qq1049859435'
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})